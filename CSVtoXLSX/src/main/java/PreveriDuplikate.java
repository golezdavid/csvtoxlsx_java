import com.aspose.cells.Workbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.MessageFormat;


public class PreveriDuplikate {


    static void preveriDuplikate() {
        File f = new File("C:\\Users\\KomunalaSG\\Desktop\\CSVtoXLSX\\src\\main\\excel");
        File[] directoryListing = f.listFiles();
        String fileName = f.getName();

        System.out.println(fileName);
        if (directoryListing != null) {
            for (File child : directoryListing) {
                String datoteka = child.toString();


                for (int i = 1; i <= 8; i++) {
                    if (datoteka.contains("KOBID")) {


                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "KOBID" + i + ".xlsx";
                            System.out.println("(" + i + ")");
                        }
                    }
                    if (datoteka.contains("VELNJ")) {
                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "VELENJE" + i + ".xlsx";
                        }
                    }

                    if (datoteka.contains("IDRJ")) {
                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "IDRIJA" + i + ".xlsx";
                        }
                    }

                    if (datoteka.contains("IZOLA")) {
                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "IZOLA" + i + ".xlsx";
                        }
                    }

                    if (datoteka.contains("GRADE")) {
                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "SGRADEC" + i + ".xlsx";
                        }
                    }

                    if (datoteka.contains("TOLMIN")) {
                        if (datoteka.contains("(" + i + ")")) {
                            fileName = "TOLMIN" + i + ".xlsx";
                        }
                    }

                }

                try {
                    String csvFileAddress = child.toString(); //csv file address
                    String xlsxFileAddress = MessageFormat.format("C:\\Users\\KomunalaSG\\Desktop\\CSVtoXLSX\\src\\main\\excel\\{0}", fileName); //xlsx file address
                    XSSFWorkbook workBook = new XSSFWorkbook();
                    XSSFSheet sheet = workBook.createSheet("sheet1");
                    String currentLine = null;
                    int RowNum = 0;
                    BufferedReader br = new BufferedReader(new FileReader(csvFileAddress));
                    while ((currentLine = br.readLine()) != null) {
                        String str[] = currentLine.split(",");
                        RowNum++;
                        XSSFRow currentRow = sheet.createRow(RowNum);
                        for (int i = 0; i < str.length; i++) {
                            currentRow.createCell(i).setCellValue(str[i]);
                        }
                        sheet.autoSizeColumn(9);
                    }

                    FileOutputStream fileOutputStream = new FileOutputStream(xlsxFileAddress);
                    workBook.write(fileOutputStream);
                    fileOutputStream.close();
                    System.out.println("Convertal datoteko v excel!");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage() + "Exception in try");
                }
            }
        } else {
            System.out.println("Directory ne obstaja!");
        }
    }
}