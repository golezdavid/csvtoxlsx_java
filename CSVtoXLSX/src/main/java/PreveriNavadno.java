import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.MessageFormat;

public class PreveriNavadno {

    static void preveriNavadno() {
        File f = new File("C:\\Users\\KomunalaSG\\Desktop\\CSVtoXLSX\\src\\main\\excel");
        File[] directoryListing = f.listFiles();
        String fileName = f.getName();

        System.out.println(fileName);
        if (directoryListing != null) {
            for (File child : directoryListing) {
                String datoteka = child.toString();


                if (datoteka.contains("KOBID")) {
                    fileName = "KOBID.xlsx";

                }
                if (datoteka.contains("VELNJ")) {
                    fileName = "VELENJE.xlsx";
                }

                if (datoteka.contains("IDRJ")) {
                    fileName = "IDRIJA.xlsx";
                }

                if (datoteka.contains("IZOLA")) {
                    fileName = "IZOLA.xlsx";
                }

                if (datoteka.contains("GRADE")) {
                    fileName = "SGRADEC.xlsx";
                }

                if (datoteka.contains("TOLMIN")) {
                    fileName = "TOLMIN.xlsx";
                }


                try {
                    String csvFileAddress = child.toString(); //csv file address
                    String xlsxFileAddress = MessageFormat.format("C:\\Users\\KomunalaSG\\Desktop\\CSVtoXLSX\\src\\main\\excel\\{0}", fileName); //xlsx file address
                    XSSFWorkbook workBook = new XSSFWorkbook();
                    XSSFSheet sheet = workBook.createSheet("sheet1");
                    String currentLine = null;
                    int RowNum = 0;
                    BufferedReader br = new BufferedReader(new FileReader(csvFileAddress));
                    while ((currentLine = br.readLine()) != null) {
                        String str[] = currentLine.split(",");
                        RowNum++;
                        XSSFRow currentRow = sheet.createRow(RowNum);
                        for (int i = 0; i < str.length; i++) {
                            currentRow.createCell(i).setCellValue(str[i]);
                        }
                        sheet.autoSizeColumn(9);
                    }

                    FileOutputStream fileOutputStream = new FileOutputStream(xlsxFileAddress);
                    workBook.write(fileOutputStream);
                    fileOutputStream.close();
                    System.out.println("Convertal datoteko v excel!");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage() + "Exception in try");
                }
            }
        } else {
            System.out.println("Directory ne obstaja!");
        }
    }

}
